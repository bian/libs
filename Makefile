OBJSDIR = obj
OUTPUTDIR = lib
HEAD = include
CXXFLAGS=-std=gnu++11 -I$(HEAD) -D_WIN32_WINNT=0x0501 -DWINVER=0x0501 -DBOOST_THREAD_USE_LIB -DBOOST_THREAD_VERSION=3
DESTLIBS=$(OUTPUTDIR)/libserialport.a

SHELLTYPE := msdos
ifeq (,$(ComSpec)$(COMSPEC))
  SHELLTYPE := posix
endif
ifeq (/bin,$(findstring /bin,$(SHELL)))
  SHELLTYPE := posix
endif

all:dirs $(DESTLIBS) 

dirs:
ifeq (posix,$(SHELLTYPE))
	@- mkdir -p $(OBJSDIR)
	@- mkdir -p $(OUTPUTDIR)
else
	@- mkdir $(subst /,\\,$(OBJSDIR))
	@- mkdir $(subst /,\\,$(OUTPUTDIR))
endif	

$(OUTPUTDIR)/libserialport.a: $(OBJSDIR)/serialport.o $(OBJSDIR)/sync_serialport.o
	ar rcs $@ $^
$(OBJSDIR)/serialport.o:serialport/serialport.cpp include/serialport.h
	g++ -c -o $@ $(CXXFLAGS) $<
$(OBJSDIR)/sync_serialport.o:serialport/sync_serialport.cc include/serialport.h
	g++ -c -o $@ $(CXXFLAGS) $<

clean:
ifeq (posix,$(SHELLTYPE))
	-@ rm -f $(OBJSDIR)/*.o $(DESTLIBS)
	-@ rm -rf $(OBJSDIR)
else
	-@ if exist $(OBJSDIR)\\*.o del $(OBJSDIR)\\*.o
	-@ if exist $(OBJSDIR) rmdir /s /q $(subst /,\\,$(OBJSDIR))
endif

