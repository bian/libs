
#include "serialport.h"


sync_serialport::sync_serialport()
: io(), serial(io)
{
}

sync_serialport::~sync_serialport()
{
	this->close();
}


bool sync_serialport::open(int Port, unsigned int br, boost_flow flowctl)
{
	this->close();
	char strport[20];
	sprintf(strport,"COM%d",Port);
	try{
		serial.open(strport);
		//serial.set_option(boost::asio::serial_port_base::baud_rate(br));
		//serial.set_option(flowctl);
                DCB dcb;
                auto handle = serial.native();
                EscapeCommFunction( handle, CLRDTR );
                //EscapeCommFunction( handle, CLRRTS );
                GetCommState(handle, &dcb);
                dcb.BaudRate = br;       
                dcb.ByteSize = 8;              
                dcb.Parity = NOPARITY;         
                dcb.StopBits = ONESTOPBIT;     
                dcb.fRtsControl = RTS_CONTROL_ENABLE ;//RTS_CONTROL_HANDSHAKE;//RTS_CONTROL_TOGGLE;
                dcb.fAbortOnError = 0;
                //dcb.fTXContinueOnXoff = 0;
                //dcb.XonLim = 100;
                //dcb.XoffLim = 100;
                SetCommState(handle, &dcb );
                EscapeCommFunction( handle, CLRDTR );
                // EscapeCommFunction( handle, CLRRTS );
                // EscapeCommFunction( handle, SETDTR );
                ClearCommBreak(handle);
                PurgeComm(handle, PURGE_RXABORT|PURGE_RXCLEAR|PURGE_TXABORT|PURGE_TXCLEAR);
                DWORD dwerror;
              COMSTAT stat;
              ClearCommError(handle,&dwerror, &stat);
                // EscapeCommFunction( handle, SETRTS );
		//serial.set_option(boost::asio::serial_port_base::parity(boost::asio::serial_port_base::parity::none));
		//serial.set_option(boost::asio::serial_port_base::stop_bits(boost::asio::serial_port_base::stop_bits::one));
		//serial.set_option(boost::asio::serial_port_base::character_size(8));
	}
	catch(boost::system::system_error& e)
    {
        // cout<<"Error: "<<e.what()<<endl;
        return false;
    }
	
	return is_open();
}

void sync_serialport::close()
{
	if (!is_open())    return;
	serial.close();
}

bool sync_serialport::write(const char * buf, size_t bufsize)
{
	if(!is_open()) return false;
	try{
		if (bufsize == -1)
		{
			bufsize = strlen(buf);
		}
		serial.write_some(boost::asio::buffer(buf, bufsize));
	}
	catch(boost::system::system_error& e)
    {
    	return false;
    }
	return true;
}

bool sync_serialport::read(char* buf, size_t bufsize)
{
	try{
		// auto handle = serial.native();
		// EscapeCommFunction( handle, SETRTS );
		boost::asio::read(serial, boost::asio::buffer(buf,bufsize));
		return true;
	}
	catch(boost::system::system_error& e)
    {
    	return false;
	// return buf.size();
    }
}

bool sync_serialport::readuntil(std::string & buf, char delimiter)
{
	try{
		boost::asio::streambuf b;
		boost::asio::read_until(serial, b, delimiter);
		std::istream is(&b);
		std::getline(is, buf, delimiter);
	}
	catch(boost::system::system_error& e)
    {
    	return false;
    }
	return true;
}
void sync_serialport::clear_read_buf(){
	boost::asio::streambuf b;
	boost::asio::read_until(serial, b, '\r');
	std::size_t sz = b.size();
	b.consume(sz); 
}
bool sync_serialport::is_open()
{
  return serial.is_open();
}
