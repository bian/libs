﻿

#include "serialport.h"


serialPort::serialPort()
{
	bOpenCom = false;
}

serialPort::~serialPort()
{
	this->close();
}

bool serialPort::open(int Port, int br)
{
	if (bOpenCom)
	{
		this->close();
		bOpenCom = false;
	}
	char szport[20];
	sprintf(szport,"\\\\.\\COM%d",Port);
	hComm = CreateFileA(szport,
                      GENERIC_READ|GENERIC_WRITE,
                      0,
                      NULL,
                      OPEN_EXISTING,
                      FILE_FLAG_OVERLAPPED,//异步，以控制超时
                      NULL);


	if (hComm == INVALID_HANDLE_VALUE)        return false;
	//设置缓冲区大小
	SetupComm(hComm, 1024, 1024);
	//设置超时
	COMMTIMEOUTS commtimeouts;
	commtimeouts.ReadIntervalTimeout = 30;
	commtimeouts.ReadTotalTimeoutConstant =2000;//固定超时
	commtimeouts.ReadTotalTimeoutMultiplier =50;//每个字符超时
	commtimeouts.WriteTotalTimeoutConstant =2000;
	commtimeouts.WriteTotalTimeoutMultiplier=50;
	SetCommTimeouts(hComm, &commtimeouts);


	DCB dcb;
	GetCommState(hComm, &dcb);
	dcb.fBinary = TRUE;
	//dcb.fParity = TRUE;
	dcb.BaudRate = br;        // 波特率 9600
	dcb.ByteSize = 8;                // 8 位数据位
	dcb.Parity = NOPARITY;            // 无奇偶校验
	dcb.StopBits = ONESTOPBIT;        // 1 个停止位
	//
	/*dcb.fDsrSensitivity = FALSE;
	dcb.fOutxCtsFlow = FALSE;
	dcb.fOutxDsrFlow = FALSE;
	dcb.fOutX = FALSE;
	dcb.fInX = FALSE;*/

	SetCommState(hComm, &dcb );

	//清空缓存
	PurgeComm(hComm,PURGE_TXCLEAR|PURGE_RXCLEAR|PURGE_RXABORT | PURGE_TXABORT);
	//
	SetCommMask(hComm, EV_RXCHAR);

	bOpenCom = true;
	return bOpenCom;
}

void serialPort::close()
{
	if (!bOpenCom)    return;

	CloseHandle(hComm);
	hComm=NULL;

}

bool serialPort::write(const char * c)
{
	if(!bOpenCom) return false;
	//DWORD    BytesSent;
	//清空错误，使I/O正常进行
	ClearCommError(hComm,NULL,NULL);

	OVERLAPPED ov={0};
	ov.hEvent=CreateEventA(NULL,TRUE,FALSE,NULL);

	BOOL hr = WriteFile(hComm,                            // Handle to COMM Port
		c,                        // Pointer to message buffer in calling finction
		strlen(c),                        // Length of message to send
		NULL,                        // Where to store the number of bytes sent
		&ov);                    // Overlapped structure
	
	if(!hr && GetLastError() == ERROR_IO_PENDING)
	{
		DWORD rest=WaitForSingleObject(ov.hEvent,1000);
		CloseHandle(ov.hEvent);
		return rest==WAIT_OBJECT_0;
	}
	CloseHandle(ov.hEvent);
	return false;
}
void serialPort::clearbuf(bool breadbuf)
{
	if(!bOpenCom) return;
	if(breadbuf)
		PurgeComm(hComm, PURGE_RXABORT|PURGE_RXCLEAR);
	else
		PurgeComm(hComm, PURGE_TXABORT|PURGE_TXCLEAR);
}
//helper
/*
 *wait forever
 */
int serialPort::waitbytes()
{
	if(!bOpenCom) return ERR_CLOSED;
	OVERLAPPED ov;
  memset(&ov,0,sizeof(OVERLAPPED));
  DWORD dwEventMask;
  int res = ERR_UNKNOWN;
  while(true)
    {
      ov.hEvent=CreateEventA(NULL,TRUE,FALSE,NULL);
      if (WaitCommEvent(hComm, &dwEventMask, &ov)) 
        {
          CloseHandle(ov.hEvent);//ClearCommError(com.hComm,NULL,NULL);
          if (dwEventMask & EV_RXCHAR) 
            {
              res= ERR_SUCCEED;
              break;
            }
        }
      else
        {
          DWORD dwRet = GetLastError();
          if( ERROR_IO_PENDING == dwRet)
            {
              WaitForSingleObject(ov.hEvent,INFINITE);
              CloseHandle(ov.hEvent);//ClearCommError(com.hComm,NULL,NULL);

              DWORD dwerror;
              COMSTAT stat;
              ClearCommError(hComm,&dwerror, &stat);

              if(stat.cbInQue==0)
                continue;
              else
                {
                  res = ERR_SUCCEED;
                  break;
                }
            }
          else 
            {
              char ss[200];
              sprintf(ss,"error code=%d",int(dwRet));
              MessageBoxA(0,ss,"ee",0);
              return res;
            }
        }
    }
  return res;

}


/*blen should be exact char length of b
 *this function will read all data in port buffer
 *timeout in second
 */
bool serialPort::read(char * b, unsigned int blen, unsigned int timeout)
{
	if(!bOpenCom) return false;
	//清空错误，使I/O正常进行
	//COMSTAT comstat;
	//DWORD errorflags;
	ClearCommError(hComm,NULL,NULL);
	//DWORD dwBytesRead=min(sizeof(ReceiveData),comstat.cbInQue);

	OVERLAPPED ov={0};
	ov.hEvent=CreateEventA(NULL,TRUE,FALSE,NULL);

	memset(b, 0, blen);//char 1 byte, 8 bits


	if (!ReadFile(hComm, b, blen, NULL, &ov)) 
	{
		if (GetLastError() == ERROR_IO_PENDING)    
		{
			DWORD rest=WaitForSingleObject(ov.hEvent, timeout);
			CloseHandle(ov.hEvent);
			return rest==WAIT_OBJECT_0;
		}
	}
	//PurgeComm(hComm, PURGE_TXABORT|PURGE_RXABORT|PURGE_TXCLEAR|PURGE_RXCLEAR);
	CloseHandle(ov.hEvent);
	return false;
}
bool serialPort::isOpened()
{
  return bOpenCom;
}
