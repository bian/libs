
#include "stdio.h"

#include "serialport.h"
//#include "win_ovlap.hpp"

#include "time.h"


void test_read(timeout_async_serial& port, char* buf)
{
    memset(buf, 0, 255);
    time_t start ,end ;
    double cost;
    time(&start);

    port.read(buf, 200, 1000);

    time(&end);
    cost=difftime(end,start);
    printf("costed time %f s\n",cost);
}

void test_read_until(timeout_async_serial& port, char* buf)
{
    memset(buf, 0, 255);
    time_t start ,end ;
    double cost;
    time(&start);

    port.read_until(buf, '\r', 2100);

    time(&end);
    cost=difftime(end,start);
    printf("costed time %f s\n",cost);
}

int main()
{

    timeout_async_serial port;
    timeout_async_serial port2;
    port.open(2);
    port2.open(3);
    port2.write("23");
    char buf[255];
    printf("\nreading\n");
    printf("%s\n", port.wait_char()?"wait success":"wait failed");

    test_read(port, buf);
    printf("get %s\n", buf);


    test_read(port, buf);
    printf("get %s\n", buf);

    printf("%s\n", port.wait_char()?"wait success":"wait failed");
    printf("%s\n", port.wait_char()?"wait success":"wait failed");

    port2.write("23564");
    test_read_until(port, buf);
    printf("get %s\n", buf);

    port2.write("23\r564");
    test_read_until(port, buf);
    printf("get %s\n", buf);

    scanf("%c", buf);

    return 0;
}
