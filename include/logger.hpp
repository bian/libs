#pragma once
#include <iostream>
#include <fstream>
#include <time.h>
#include <string.h>

#define logfile "log.txt"

class logger: public std::ofstream
{
public:
	logger(){
		this->open(logfile, ios_base::app);
		(*this) << "Log system initialized.." << std::endl;
	}
	~logger(){
		this->close();
	}
	template < typename type >
	std::ostream& operator<<(type data)
	{
		time(&now);
		char * t = ctime(&now);
		static char mt[30];
		strncpy(mt, t, strlen(t)-1);
		return std::ostream::write("[",1)<< mt << "]: "<<data;

	}
private:
		time_t now;

};
