#pragma once

#include "windows.h"

/*
Manual reset event
*/
class EventWrapper
{
public:
    EventWrapper()
        :event_(CreateEventA(NULL,TRUE,FALSE,NULL))
    {}

    ~EventWrapper()
    {
        ::CloseHandle(event_);
    }

    HANDLE GetEvent()
    {
        return event_;
    }



private:
    HANDLE event_;



};