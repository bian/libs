#pragma once

#include "windows.h"


#include "win_event.hpp"


/*
auto included manual reset event
*/
class OverlapperWrapper: public OVERLAPPED
{
public:
    OverlapperWrapper()
    {
        Internal = 0;
        InternalHigh = 0;
        Offset = 0;
        OffsetHigh = 0;
        hEvent = evt_.GetEvent();
    }

    ~OverlapperWrapper()
    {
    }

private:
    EventWrapper evt_;
};