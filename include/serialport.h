﻿#pragma once

#include "stdio.h"
#include "windows.h"
#include <string>

#include "win_ovlap.hpp"

#define msg_ul(d) do{char st[255]; sprintf(st,"%lu",d); ::MessageBoxA(0,st,0,0);}while(0)

const int READ_TIMEOUT = 100;

class timeout_async_serial  
{

public:

    timeout_async_serial()
        :bOpenCom(false)
    {
    };

    virtual ~timeout_async_serial(){
        this->close();
    };

    bool open(int Port, int br = 9600){
        if (bOpenCom)
        {
            this->close();
            bOpenCom = false;
        }

        char szport[15];
        sprintf(szport,"\\\\.\\COM%d",Port);

        hComm = CreateFileA(szport,
            GENERIC_READ|GENERIC_WRITE,
            0,
            NULL,
            OPEN_EXISTING,
            FILE_FLAG_OVERLAPPED,
            NULL);

        if (hComm == INVALID_HANDLE_VALUE)        return false;
        
        SetupComm(hComm, 1024, 1024);

        COMMTIMEOUTS commtimeouts;
        commtimeouts.ReadIntervalTimeout = 10;
        commtimeouts.ReadTotalTimeoutConstant =0;
        commtimeouts.ReadTotalTimeoutMultiplier =0;
        commtimeouts.WriteTotalTimeoutConstant =0;
        commtimeouts.WriteTotalTimeoutMultiplier=0;
        SetCommTimeouts(hComm, &commtimeouts);

        DCB dcb;
        GetCommState(hComm, &dcb);
        dcb.fBinary = TRUE;
        //dcb.fParity = TRUE;
        dcb.BaudRate = br;        
        dcb.ByteSize = 8;         
        dcb.Parity = NOPARITY;    
        dcb.StopBits = ONESTOPBIT;
        //
        /*dcb.fDsrSensitivity = FALSE;
        dcb.fOutxCtsFlow = FALSE;
        dcb.fOutxDsrFlow = FALSE;
        dcb.fOutX = FALSE;
        dcb.fInX = FALSE;*/

        SetCommState(hComm, &dcb );

        DWORD dwerror;
        COMSTAT stat;
        ClearCommError(hComm,&dwerror, &stat);

        PurgeComm(hComm,PURGE_TXCLEAR|PURGE_RXCLEAR|PURGE_RXABORT | PURGE_TXABORT);
        //
        SetCommMask(hComm, EV_RXCHAR);

        bOpenCom = true;
        return bOpenCom;
    };


    bool read_until(char * b, const char end_char = '\r', const unsigned int timeout=READ_TIMEOUT)
    {
        if(!bOpenCom) return false;

        bool res = read(b, 1, timeout);
        if(!res) return false;

        if(b[0] != end_char)
            read_until(b+1, end_char, timeout);

        return true;
    };


    bool read(char * b, unsigned int byte_num_to_read, const unsigned int timeout=READ_TIMEOUT){
        if(!bOpenCom) return false;

        OverlapperWrapper ov;
        if (ReadFile(hComm, b, byte_num_to_read, NULL, &ov)) return true;

        if (GetLastError() == ERROR_IO_PENDING)    
        {
            DWORD result=WaitForSingleObject(ov.hEvent, timeout);
            if(result != WAIT_OBJECT_0)
            {
                CancelIo(hComm);
                return false;
            }
            return true;
        }

        return false;
    };


    void close(){
        if (!bOpenCom)    return;
        clearbuf();
        CloseHandle(hComm);
        hComm=NULL;
    };


    bool write(const char * c, const unsigned int timeout=100){
        if(!bOpenCom) return false;

        OverlapperWrapper ov;

        BOOL hr = WriteFile(hComm,                
            c,                    
            strlen(c),            
            NULL,                 
            &ov);                 

        if(hr == TRUE) return true;
        
        if(GetLastError() == ERROR_IO_PENDING)
        {
            DWORD result=WaitForSingleObject(ov.hEvent, timeout);
            if(result != WAIT_OBJECT_0)
            {
                CancelIo(hComm);
                return false;
            }
            return true;
        }

        return false;
    }

    void clearbuf(){
        if(!bOpenCom) return;
        PurgeComm(hComm, PURGE_RXABORT|PURGE_RXCLEAR|PURGE_TXABORT|PURGE_TXCLEAR);
    }

    bool isOpened()
    {
        return bOpenCom;
    }

    //return true if empty
    bool check_buf_empty()
    {
        DWORD err;
        COMSTAT stt;
        ClearCommError(hComm, &err, &stt);
        //printf("buffered  %d\n", stt.cbInQue);
        return stt.cbInQue == 0;
    }


    bool wait_char()
    {
        OverlapperWrapper ov;
        DWORD evt_mask;
        if(WaitCommEvent(hComm, &evt_mask, &ov) && (evt_mask & EV_RXCHAR))
        {
            if(check_buf_empty())
                return wait_char();
            else
                return true;
        }
        
        if(GetLastError() == ERROR_IO_PENDING)
        {
            DWORD result=WaitForSingleObject(ov.hEvent, INFINITE);
            if(result != WAIT_OBJECT_0)
            {
                CancelIo(hComm);
                return false;
            }
            if(evt_mask & EV_RXCHAR)
                if(check_buf_empty())
                    return wait_char();
                else
                    return true;
        }
        return false;
    }

private:

    HANDLE hComm;
    bool bOpenCom;
};




class sync_serial  
{
public:
    sync_serial(){
        bOpenCom = false;
    };
    virtual ~sync_serial(){
        this->close();
    };

    bool open(int Port, int br = 9600){
        if (bOpenCom)
        {
            this->close();
            bOpenCom = false;
        }
        char szport[15];
        sprintf(szport,"\\\\.\\COM%d",Port);

        hComm = CreateFileA(szport,
            GENERIC_READ|GENERIC_WRITE,
            0,
            NULL,
            OPEN_EXISTING,
            FILE_ATTRIBUTE_NORMAL,
            NULL);

        if (hComm == INVALID_HANDLE_VALUE)        return false;

        DCB dcb;
        GetCommState(hComm, &dcb);

        SetupComm(hComm, 1024, 1024);
        COMMTIMEOUTS commtimeouts;
        commtimeouts.ReadIntervalTimeout = 10;
        commtimeouts.ReadTotalTimeoutConstant =0;
        commtimeouts.ReadTotalTimeoutMultiplier =0;
        commtimeouts.WriteTotalTimeoutConstant =0;
        commtimeouts.WriteTotalTimeoutMultiplier=0;
        SetCommTimeouts(hComm, &commtimeouts);

        dcb.fBinary = TRUE;
        //dcb.fParity = TRUE;
        dcb.BaudRate = br;        
        dcb.ByteSize = 8;         
        dcb.Parity = NOPARITY;    
        dcb.StopBits = ONESTOPBIT;
        //
        /*dcb.fDsrSensitivity = FALSE;
        dcb.fOutxCtsFlow = FALSE;
        dcb.fOutxDsrFlow = FALSE;
        dcb.fOutX = FALSE;
        dcb.fInX = FALSE;*/

        SetCommState(hComm, &dcb );

        DWORD dwerror;
        COMSTAT stat;
        ClearCommError(hComm,&dwerror, &stat);

        PurgeComm(hComm,PURGE_TXCLEAR|PURGE_RXCLEAR|PURGE_RXABORT | PURGE_TXABORT);
        //
        SetCommMask(hComm, EV_RXCHAR);

        bOpenCom = true;
        return bOpenCom;
    };


    bool read_until(char * b, const char end='\r'){
        if(!bOpenCom) return false;
        while(1){
            if(!read_one_char(b))
                return false;
            if(b[0] == end)
                return true;
            b++;
        }
        return true;
    };

    bool read(char * b, unsigned int blen){
        if(!bOpenCom) return false;

        while(blen>0){
            if(!read_one_char(b))
                return false;
            blen--;
            b++;
        }
        return true;
    };
    void close(){
        if (!bOpenCom)    return;
        CloseHandle(hComm);
        hComm=NULL;
        bOpenCom = false;
    };
    bool write(const char * c){
        if(!bOpenCom) return false;
        DWORD d;
        BOOL hr = WriteFile(hComm,
            c,
            strlen(c),
            &d,
            NULL);

        return true;
    };

    void clearbuf(){
        if(!bOpenCom) return;
        PurgeComm(hComm, PURGE_RXABORT|PURGE_RXCLEAR|PURGE_TXABORT|PURGE_TXCLEAR);
    };

    HANDLE hComm;
    bool isOpened(){return bOpenCom;};
private:
    bool read_one_char(char * b){
        if(!bOpenCom) return false;
        static DWORD d;
        return ReadFile(hComm, b, 1, &d, NULL)==TRUE;
    };
    bool bOpenCom;
};