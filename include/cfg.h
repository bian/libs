﻿//2011-6-3

#pragma once
#include <fstream>
#include <map>
#include "windows.h"
//#include "const.h"

#define NOVALUE ""
#define ARRAY_SIZE 256


class cfg
{
public:
	cfg(const char *path)
	{
		strcpy(path_, path);
		strcpy(cur_region_name_, "APP");
		std::ofstream fout( path, std::ios_base::app);	
		if ( fout ) fout.close();
	}
	virtual ~cfg(void)
	{
	}

	void set_region_name(const char*region_name){
		strcpy(cur_region_name_, region_name);
	}

	void set(const char* key,const char*value)
	{
		WritePrivateProfileStringA(cur_region_name_,key,value,path_);
	}
	void set_int(const char* key,int value)
	{
		char s[50];
		sprintf(s, "%d", value);
		WritePrivateProfileStringA(cur_region_name_,key,s,path_);
	}
	void set_bool(const char* key,bool value)
	{
		WritePrivateProfileStringA(cur_region_name_,key, value?"1":"0",path_);
	}
	bool get(const char* key, char*value, const char* defaultstr = NOVALUE)
	{
		return 0 != GetPrivateProfileStringA(cur_region_name_,key,defaultstr,value, ARRAY_SIZE ,path_);
	}
	int get_int(const char* key, const int & default_int)
	{
		static char dest[ARRAY_SIZE];
		static char default_str[ARRAY_SIZE];
		sprintf(default_str, "%d", default_int);
		GetPrivateProfileStringA(cur_region_name_,key,default_str,dest, ARRAY_SIZE ,path_);
		return atoi(dest);
	}
	bool get_bool(const char* key, const bool & default_bool)
	{
		static char dest[ARRAY_SIZE];
		static char default_str[ARRAY_SIZE];
		sprintf(default_str, "%d", default_bool?1:0);
		GetPrivateProfileStringA(cur_region_name_,key,default_str,dest, ARRAY_SIZE ,path_);
		return atoi(dest) != 0;
	}

private:
	char path_[ARRAY_SIZE];
	char cur_region_name_[ARRAY_SIZE];

};

